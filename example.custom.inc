<?php 
/**
 * example.custom.inc 
 * Rename this file to custom.inc and put here your hooks and functions
 * Feel free to edit this file
 */

/**
 * Implementation of hook_init().
 */
function adjust_init() {
  if (file_exists(dirname(__FILE__) . '/custom.css')) {
    drupal_add_css(drupal_get_path('module', 'adjust') .'/custom.css');  // Include custom CSS file
  }
  else {
    drupal_set_message('Adjust module: please rename "example.custom.css" file to "custom.css"', 'error');
  }
  //setlocale(LC_CTYPE, 'ru_RU.UTF-8'); // Фикс для нормальной загрузки файлов с русскими именами
}

/**
 *  Example function: hide "Homepage" field in comments
 */ /* 
function adjust_form_alter(&$form, &$form_state, $form_id){
  global $user;
  if (!$user->uid && $form_id == 'comment_form') 
  {
    unset($form['homepage']);
  }
}
*/
